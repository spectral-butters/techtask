package io.mobilt.techtest;

import io.mobilt.techtest.entities.Record;
import io.mobilt.techtest.entities.Status;
import io.mobilt.techtest.repository.RecordRepository;
import io.mobilt.techtest.repository.specifications.RecordFilterSpecification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SpecificationFilterTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RecordRepository recordRepository;

    @Before
    public void loadData(){
        for (int i = 0; i < 2; i++) {
            Record r = new Record();
            r.setId("id" + i);
            r.setLiveStatus("inprogress");
            Status status = new Status();
            status.setType("HT");
            r.setStatus(status);
            entityManager.persist(r);
        }

        for (int i = 0; i < 4; i++) {
            Record r = new Record();
            r.setId("id" + i + 10);
            r.setLiveStatus("cancelled");
            Status status = new Status();
            status.setType("FT");
            r.setStatus(status);
            entityManager.persist(r);
        }

        for (int i = 0; i < 5; i++) {
            Record r = new Record();
            r.setId("id" + i + 20);
            r.setLiveStatus("inprogress");
            Status status = new Status();
            status.setType("FT");
            r.setStatus(status);
            entityManager.persist(r);
        }

        entityManager.flush();
    }

    @Test
    public void onlyInprogressRecordsShouldBeReturned(){
        RecordFilterSpecification liveStatusOnlySpecification = new RecordFilterSpecification();
        liveStatusOnlySpecification.setLiveStatus("inprogress");

        Page<Record> liveStatusRecords = recordRepository.findAll(liveStatusOnlySpecification, Pageable.unpaged());

        assertEquals(7, liveStatusRecords.getTotalElements());

    }

    @Test
    public void onlyFTStatusTypeRecordsShouldBeReturned(){
        RecordFilterSpecification statusTypeSpecification = new RecordFilterSpecification();
        statusTypeSpecification.setStatusType("FT");

        Page<Record> statusTypeRecords = recordRepository.findAll(statusTypeSpecification, Pageable.unpaged());

        assertEquals(9, statusTypeRecords.getTotalElements());
    }

    @Test
    public void onlyFTAndInprogressRecordsShouldBeReturned(){
        RecordFilterSpecification specification = new RecordFilterSpecification();
        specification.setStatusType("FT");
        specification.setLiveStatus("inprogress");

        Page<Record> records = recordRepository.findAll(specification, Pageable.unpaged());

        assertEquals(5, records.getTotalElements());
    }

}
