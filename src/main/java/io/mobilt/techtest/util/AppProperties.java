package io.mobilt.techtest.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("app.props")
public class AppProperties {
    private String fileName;
}
