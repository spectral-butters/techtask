package io.mobilt.techtest.controller;

import io.mobilt.techtest.entities.Record;
import io.mobilt.techtest.service.interfaces.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/record")
public class RecordController {

    private final RecordService recordService;

    @Autowired
    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping
    public ResponseEntity<List<Record>> getRecords(@RequestParam(required = false) String liveStatus,
                                                   @RequestParam(required = false) String statusType,
                                                   Pageable pageable){

        List<Record> records = recordService.findRecords(liveStatus, statusType, pageable);
        return ResponseEntity.ok(records);
    }

}
