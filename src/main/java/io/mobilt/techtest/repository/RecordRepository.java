package io.mobilt.techtest.repository;

import io.mobilt.techtest.entities.Record;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordRepository extends JpaRepository<Record, String> {

    Page<Record> findAll(Specification<Record> specification, Pageable pageable);

}
