package io.mobilt.techtest.repository.specifications;

import io.mobilt.techtest.entities.Record;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Setter
public class RecordFilterSpecification implements Specification<Record> {

    private String statusType;
    private String liveStatus;


    /**
     * Method to create a predicate for Record entity, which will be used for filtering query result
     * @param root
     * @param criteriaQuery
     * @param criteriaBuilder
     * @return
     */
    @Override
    public Predicate toPredicate(Root<Record> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Predicate result = criteriaBuilder.conjunction();

        if (this.statusType != null) {
            result = criteriaBuilder.and(result, criteriaBuilder.equal(root.get("status").<String> get("type"), this.statusType));
        }

        if (this.liveStatus != null) {
            result = criteriaBuilder.and(result, criteriaBuilder.equal(root.get("liveStatus"), this.liveStatus));
        }

        return result;

    }
}
