package io.mobilt.techtest.service.interfaces;

import io.mobilt.techtest.entities.Record;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RecordService {

    List<Record> findRecords(String liveStatus, String statusType, Pageable pageable);

}
