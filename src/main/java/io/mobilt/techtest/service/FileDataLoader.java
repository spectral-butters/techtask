package io.mobilt.techtest.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.mobilt.techtest.entities.Record;
import io.mobilt.techtest.exception.TestDataException;
import io.mobilt.techtest.repository.RecordRepository;
import io.mobilt.techtest.util.AppProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileDataLoader {

    private final RecordRepository recordRepository;
    private final AppProperties appProperties;

    @Autowired
    public FileDataLoader(RecordRepository recordRepository, AppProperties appProperties) {
        this.recordRepository = recordRepository;
        this.appProperties = appProperties;
    }

    /**
     * Method to load test data into database.
     * If record table is not empty - nothing will be added.
     */
    @PostConstruct
    private void init() throws TestDataException {
        long count = recordRepository.count();
        if (count > 0)
            return;

        ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        om.findAndRegisterModules();

        String json;
        try {
            json = getResourceFileAsString(appProperties.getFileName());
        } catch (IOException e) {
            throw new TestDataException("Error occur while reading test data file. Please make sure that correct filePath is specified in application.properties.", e);
        }

        try {
            List<Record> records = om.readValue(json, new TypeReference<List<Record>>(){});
            System.out.println("Amount of records: " + records.size());
            recordRepository.saveAll(records);
        } catch (IOException e) {
            throw new TestDataException("Error occur while parsing json file. Are format and filePath correct? ", e);
        }
    }

    private String getResourceFileAsString(String fileName) throws IOException {
        try (InputStream is = TypeReference.class.getResourceAsStream(fileName)) {
            if (is == null) return null;
            try (InputStreamReader isr = new InputStreamReader(is);
                 BufferedReader reader = new BufferedReader(isr)) {
                return reader.lines().collect(Collectors.joining(System.lineSeparator()));
            }
        }
    }

}
