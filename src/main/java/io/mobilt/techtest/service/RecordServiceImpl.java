package io.mobilt.techtest.service;

import io.mobilt.techtest.entities.Record;
import io.mobilt.techtest.repository.RecordRepository;
import io.mobilt.techtest.repository.specifications.RecordFilterSpecification;
import io.mobilt.techtest.service.interfaces.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordServiceImpl implements RecordService {

    private final RecordRepository recordRepository;

    @Autowired
    public RecordServiceImpl(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    @Override
    public List<Record> findRecords(String liveStatus, String statusType, Pageable pageable) {
        RecordFilterSpecification specification = new RecordFilterSpecification();
        specification.setLiveStatus(liveStatus);
        specification.setStatusType(statusType);
        Page<Record> page = recordRepository.findAll(specification, pageable);
        return page.getContent();
    }
}
