package io.mobilt.techtest.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@Entity
public class Team {

    @Id
    private Long id;

    private String name;

    private String slug;

    private String gender;

}
