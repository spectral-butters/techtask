**Technical test task for Mobilt.**

On the first launch - app fetches data from json, that contains dataset of games' records and saves it into database.

To change file name of test data json, please refer to `application.properties` 

In total - application covers one endpoint for getting and filtering records' data.
 

**Running the app**

To run the app - execute (Assuming you are running Docker)

`docker-compose up` 

_Note: to launch the app successfully - your 8080 and 5432 ports must be available. Docker deploys on **localhost:8080**_ 

If you are not running Docker
1. Install and run PostgreSQL server
2. Create new database
3. In `application.properties` set url, password and username according to your setup.
4. Run `mvn clean package`
5. Run `java -jar target/tech-test-1.0-SNAPSHOT.jar`

**API Usage**

Name: **/api/record**

Description: **Return game records based on provided filters** 

Example: 

**/api/record/?statusType=finished&liveStatus=FT&page=1&size=3&sort=time,asc** 

Method: **GET**

Params:

**liveStatus** (optional) : Describes current live game status
 
 Can be FT, HT, -, Cancelled, or minute (example 60+) etc.
 
 **statusType** (optional) : Describes general game status
  
 Can be inprogress, notstarted, finished, canceled 
 
 **page** (optional)  : Specify page that you want to fetch (to reduce latency)
 
 **size** (optional) (default = 1000) : Specify amount of records on one page
 
 **sort** (optional) : Specify Record variable, based on which you result will be sorted. `asc` for ascending, `desc` for descending    

